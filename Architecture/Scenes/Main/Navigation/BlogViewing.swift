//
//  BlogViewing.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import Foundation

protocol BlogViewing: Coordinator {
    func showBlog()
}
