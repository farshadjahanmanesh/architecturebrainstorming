//
//  MainCoordinator.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import UIKit

final class MainCoordinator: NSObject, Coordinator, BlogViewing, UINavigationControllerDelegate {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var storyboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: .main)
    }
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        super.init()
        self.navigationController.delegate = self
    }
    
    func start() {
        let viewController = MainViewController.instantiate(in: storyboard)
        viewController.viewModel = .init(title: "Main")
        viewController.coordinator = self
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func showBlog() {
        let blogCoordinator = BlogCoordinator(navigationController: navigationController)
        childCoordinators.append(blogCoordinator)
        blogCoordinator.start()
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let transitionCoordinator = navigationController.transitionCoordinator,
            let viewController = transitionCoordinator.viewController(forKey: .from),
            !navigationController.viewControllers.contains(viewController)
        else { return }

        if let blogViewController = viewController as? BlogViewController {
            childDidFinish(blogViewController.coordinator)
        }
    }
}
