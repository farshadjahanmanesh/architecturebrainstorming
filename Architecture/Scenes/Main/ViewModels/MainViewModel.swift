//
//  MainViewModel.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import Foundation
import RxSwift

final class MainViewModel: ViewModel, ViewModelProtocol {
    
    let title: BehaviorSubject<String>
    
    struct Input {
        let tappedBlog: Observable<BlogViewing>
    }
    
    struct Output {
        let title: BehaviorSubject<String>
    }

    init(title: String) {
        self.title = .init(value: title)
    }
    
    func transform(input: Input) -> Output {
        input.tappedBlog
            .subscribe(onNext: { coordinator in
                coordinator.showBlog()
            }).disposed(by: disposeBag)
        
        return Output(title: title)
    }
}

