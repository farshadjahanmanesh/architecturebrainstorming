//
//  MainViewController.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import UIKit
import RxCocoa

final class MainViewController: ViewController<MainCoordinator, MainViewModel> {
    
    @IBOutlet private var blogButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }
    
    func bindViewModel() {
        let output = viewModel.transform(
            input: .init(
                tappedBlog: blogButton.rx
                    .tap
                    .compactMap { [unowned self] _ in return self.coordinator }
                    .share()
            )
        )
        
        output.title
            .bind(to: navigationItem.rx.title)
            .disposed(by: disposeBag)
    }
}
