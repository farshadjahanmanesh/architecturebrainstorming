//
//  PostCell.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import UIKit
import RxCocoa
import RxSwift

final class PostCell: UITableViewCell, Reusable {

    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    private var disposeBag = DisposeBag()

    var viewModel: PostViewModel! {
        willSet {
           let output = newValue.transform(input: .init())
            
            output.title
                .bind(to: titleLabel.rx.text)
                .disposed(by: disposeBag)
            
            output.description
                .bind(to: descriptionLabel.rx.text)
                .disposed(by: disposeBag)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
}
