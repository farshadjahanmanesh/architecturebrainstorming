//
//  CategoryCell.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import UIKit
import RxSwift
import RxCocoa

class CategoryCell: UICollectionViewCell, Reusable {

    @IBOutlet private var titleLabel: UILabel!
    private var disposeBag = DisposeBag()

    var viewModel: CategoryViewModel! {
        willSet {
           let output = newValue.transform(input: .init())
            
            output.title
                .bind(to: titleLabel.rx.text)
                .disposed(by: disposeBag)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
}
