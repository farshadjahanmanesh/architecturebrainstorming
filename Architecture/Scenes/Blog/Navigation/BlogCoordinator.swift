//
//  BlogCoordinator.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import UIKit

final class BlogCoordinator: Coordinator, PostViewing {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var storyboard: UIStoryboard {
        return UIStoryboard(name: "Blog", bundle: .main)
    }
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewController = BlogViewController.instantiate(in: storyboard)
        viewController.viewModel = .init(service: .shared)
        viewController.coordinator = self
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func show(post: Post) {
        let viewController = PostViewController.instantiate(in: storyboard)
        viewController.viewModel = .init(post: post)
        viewController.coordinator = self
        navigationController.pushViewController(viewController, animated: true)
    }
}
