//
//  PostViewing.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import Foundation

protocol PostViewing {
    func show(post: Post)
}
