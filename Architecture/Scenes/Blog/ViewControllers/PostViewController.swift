//
//  PostViewController.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import UIKit

class PostViewController: ViewController<BlogCoordinator, PostViewModel> {

    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }
    
    func bindViewModel() {
        let output = viewModel.transform(input: .init())
     
        output.title
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
        
        output.description
            .bind(to: descriptionLabel.rx.text)
            .disposed(by: disposeBag)
    }
}
