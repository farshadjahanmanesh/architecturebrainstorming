//
//  BlogViewController.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import UIKit

final class BlogViewController: ViewController<BlogCoordinator, BlogViewModel> {
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        registerViews()
        bindViewModel()
    }
    
    func registerViews() {
        collectionView.register(CategoryCell.nib, forCellWithReuseIdentifier: CategoryCell.reuseID)
        tableView.register(PostCell.nib, forCellReuseIdentifier: PostCell.reuseID)
    }
    
    func bindViewModel() {
        
        let output = viewModel.transform(
            input: .init(
                didSelectRow: tableView.rx.itemSelected
                    .compactMap({ [unowned self] in ($0.row, self.coordinator) as? (Int, PostViewing) })
                    .share()
            )
        )
        
        output.categories
            .bind(to: collectionView.rx.items(cellIdentifier: CategoryCell.reuseID, cellType: CategoryCell.self)) { row, viewModel, cell in
                cell.viewModel = viewModel
            }.disposed(by: disposeBag)
        
        output.posts
            .bind(to: tableView.rx.items(cellIdentifier: PostCell.reuseID, cellType: PostCell.self)) { row, viewModel, cell in
                cell.viewModel = viewModel
            }.disposed(by: disposeBag)
    }
}
