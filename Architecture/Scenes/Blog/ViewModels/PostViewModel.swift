//
//  PostViewModel.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import Foundation
import RxSwift

class PostViewModel: ViewModel, ViewModelProtocol {
    
    private let post: Post
    private let title: BehaviorSubject<String>
    private let description: BehaviorSubject<String>

    struct Input {
            
    }
    
    struct Output {
        let title: Observable<String>
        let description: Observable<String>
    }
    
    func transform(input: Input) -> Output {
        return Output(title: title.share(), description: description.share())
    }
    
    init(post: Post) {
        self.post = post
        self.title = .init(value: post.title)
        self.description = .init(value: post.description)
    }
}
