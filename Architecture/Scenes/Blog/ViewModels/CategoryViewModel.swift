//
//  CategoryViewModel.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import Foundation
import RxSwift

class CategoryViewModel: ViewModel, ViewModelProtocol {
    
    private let category: Category
    private let title: BehaviorSubject<String>

    struct Input {
            
    }
    
    struct Output {
        let title: Observable<String>
    }
    
    func transform(input: Input) -> Output {
        return Output(title: title.share())
    }
    
    init(category: Category) {
        self.category = category
        self.title = .init(value: category.title)
    }
}
