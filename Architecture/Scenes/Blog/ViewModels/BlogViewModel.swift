//
//  BlogViewModel.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import Foundation
import RxSwift

final class BlogViewModel: ViewModel, ViewModelProtocol {
    
    private let service: BlogService
    private var blog: Blog
    private let categories: BehaviorSubject<[CategoryViewModel]>
    private let posts: BehaviorSubject<[PostViewModel]>
    
    struct Input {
        let didSelectRow: Observable<(Int, PostViewing)>
    }
    
    struct Output {
        let categories: Observable<[CategoryViewModel]>
        let posts: Observable<[PostViewModel]>
    }
    
    init(service: BlogService) {
        self.service = service
        self.blog = service.getBlog()
        self.categories = .init(value: service.getCategories().map(CategoryViewModel.init(category:)))
        self.posts = .init(value: service.getPosts().map(PostViewModel.init(post:)))
    }
    
    func transform(input: Input) -> Output {
        input.didSelectRow
            .subscribe(onNext: { [weak self] row, coordinator in
                guard let self = self else { return }
                coordinator.show(post: self.blog.posts[row])
            }).disposed(by: disposeBag)
        
        return Output.init(
            categories: categories.share(),
            posts: posts.share())
    }
}
