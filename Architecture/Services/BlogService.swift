//
//  BlogService.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import Foundation

struct BlogService {
    
    private init() { }
    static let shared = BlogService()
    
    func getBlog() -> Blog {
        return Blog(categories: getCategories(), posts: getPosts())
    }
    
    func getPosts() -> [Post] {
        return Post.allObjects()
    }
    
    func getCategories() -> [Category] {
        return [.news, .sport]
    }
}
