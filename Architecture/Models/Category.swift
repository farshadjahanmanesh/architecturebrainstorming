//
//  Category.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import Foundation

struct Category {
    
    let id: UUID = .init()
    let title: String
    
    static var news: Category {
        return .init(title: "News")
    }
    
    static var sport: Category {
        return .init(title: "Sport")
    }
}
