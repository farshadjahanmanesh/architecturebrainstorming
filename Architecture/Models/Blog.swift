//
//  Blog.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import Foundation

struct Blog {
    
    let id: UUID = .init()
    let categories: [Category]
    let posts: [Post]
}
