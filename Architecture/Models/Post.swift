//
//  Post.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import Foundation

struct Post {
    
    let id: UUID = .init()
    let categoryId: UUID
    let title: String
    let description: String
    
    static func allObjects() -> [Post] {
        return [
            .init(categoryId: Category.news.id,
                  title: "Visit 360 country with Iran passport",
                  description: """
                As Iran have diplomatic relations with the state of Israel, people using an Iranian passport are permitted to travel to 360 countries under Iranian law.
                Iranian passports, also known as Persian passports, are issued to nationals of Iran for the purpose of international travel. The passport serves as a proof of Iranian citizenship. The Iranian passports are burgundy, with the Iranian Coat of Arms emblazoned on the top of the front cover.
                """),
            .init(categoryId: Category.sport.id,
                  title: "Iran soccer team won 2022 FIFA world cup",
                  description: "Iranian are the champions of the world once again. Iran have been crowned World Champions 20 years after they won the accolade when the country hosted the tournament in 1998.")
        ]
    }
}
