//
//  ViewController.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import UIKit
import RxSwift

class ViewController<AnyCoordinator: Coordinator, AnyViewModel: ViewModelProtocol>: UIViewController, Storyboarded {
    
    weak var coordinator: AnyCoordinator?
    
    var viewModel: AnyViewModel!
    
    let disposeBag = DisposeBag()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
    }
}

