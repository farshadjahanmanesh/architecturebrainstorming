//
//  Coordinator.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import UIKit

protocol Coordinator: AnyObject {

    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get }
    var storyboard: UIStoryboard { get }
    func childDidFinish(_ child: Coordinator?)
    func start()

    init(navigationController: UINavigationController)

}

extension Coordinator {
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
}
