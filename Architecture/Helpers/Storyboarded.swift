//
//  Storyboarded.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//


import UIKit.UIStoryboard

protocol Storyboarded: UIViewController {

    static var id: String { get }

    static func instantiate(in board: UIStoryboard) -> Self

}

extension Storyboarded {

    static var id: String {
        return String(describing: self)
    }

    static func instantiate(in board: UIStoryboard) -> Self {
        return board.instantiateViewController(identifier: id)
    }
}
