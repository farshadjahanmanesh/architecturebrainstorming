//
//  Reusable.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import UIKit

protocol Reusable: UIView {
    static var reuseID: String { get }
    static var nib: UINib { get }
}

extension Reusable {
    
    static var reuseID: String {
        return String(describing: Self.self)
    }
    
    static var nib: UINib {
        return UINib(nibName: reuseID, bundle: .main)
    }
}

