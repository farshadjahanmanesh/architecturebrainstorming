//
//  ViewModel.swift
//  Architecture
//
//  Created by Mohammadreza on 11/1/20.
//

import RxSwift

class ViewModel {
    
    let disposeBag: DisposeBag = .init()
}

protocol ViewModelProtocol {
    associatedtype Input
    associatedtype Output
    func transform(input: Input) -> Output
}
